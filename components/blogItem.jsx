import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const BlogItem = () => {
    return (
        <View style={styles.main}>
            <Image style={styles.image} source={{ uri: 'https://letsenhance.io/static/8f5e523ee6b2479e26ecc91b9c25261e/1015f/MainAfter.jpg' }} />
            <Text style={styles.title}> Chameleon </Text>
            <Text style={styles.description}> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, consectetur fugiat. Dolores voluptatum officia... </Text>
        </View>
    )
}

export default BlogItem

const styles = StyleSheet.create({

    main: {
        width: '48.3%',
        backgroundColor: 'white',
        paddingBottom: 10,
    },

    image: {
        width: '100%',
        height: 150,
        objectFit: 'cover'
    },

    title: {
        paddingTop: 5,
        fontWeight: '700',
        paddingHorizontal: 10
    },

    description: {
        paddingHorizontal: 10,
        fontSize: 12,
        color: 'gray',
        paddingBottom: 5,
    }

})
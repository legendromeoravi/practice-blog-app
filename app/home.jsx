import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import BlogItem from '../components/blogItem'

const Home = () => {
    return (
        <View style={{ flex: 1 }}>

            <View style={styles.blogContainer}>
                <BlogItem />
                <BlogItem />
                <BlogItem />
            </View>


        </View>
    )
}

export default Home

const styles = StyleSheet.create({

        blogContainer: {
            flex: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            gap: 10,
            justifyContent: 'flex-start',
            marginHorizontal: '5%',
            marginVertical: 10
        }

})